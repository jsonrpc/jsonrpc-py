# JSON-RPC Python

[![pipeline status][pipeline]][homepage]
[![coverage report][coverage]][homepage]
[![latest release][version]][pypi]
[![pypi downloads][downloads]][pypi]

**JSON-RPC Python** is a lightweight ASGI micro framework for building web applications.

## Features

* Pure zero-dependency JSON-RPC 2.0 and OpenRPC implementation.
* Simple and user-friendly API interface.
* 100% type annotated codebase.
* 100% test coverage, so it's production-ready.

## Requirements

Python 3.12 and above.

## Installation

```shell
$ pip install jsonrpc-py
```

## Distribution

This project is licensed under either of [Apache License 2.0](LICENSE-APACHE) or [MIT License](LICENSE-MIT) at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache License 2.0, shall be
dual licensed as above, without any additional terms or conditions.

## Links

* Documentation: <https://docs.jsonrpc.ru>
* PyPI Releases: <https://pypi.org/project/jsonrpc-py>
* Source Code: <https://gitlab.com/jsonrpc/jsonrpc-py>

[homepage]: <https://gitlab.com/jsonrpc/jsonrpc-py>
[pipeline]: <https://img.shields.io/gitlab/pipeline-status/jsonrpc/jsonrpc-py?branch=main&logo=gitlab&style=flat-square>
[coverage]: <https://img.shields.io/gitlab/pipeline-coverage/jsonrpc/jsonrpc-py?branch=main&logo=gitlab&style=flat-square>
[pypi]: <https://pypi.org/project/jsonrpc-py>
[version]: <https://img.shields.io/pypi/v/jsonrpc-py?color=steelblue&logo=python&logoColor=steelblue&style=flat-square>
[downloads]: <https://img.shields.io/pypi/dm/jsonrpc-py?color=steelblue&logo=python&logoColor=steelblue&style=flat-square>
