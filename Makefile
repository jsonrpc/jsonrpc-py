.PHONY: all help install build release docs ruff mypy linting test coverage clean

all: install clean

help: ## Display this help screen
	@grep -E '^[a-zA-Z0-9_]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'

install: ## Install a dependencies for development
	@uv sync --all-extras

build: ## Build a package
	@uv build --sdist

release: ## Upload a package to the PyPI
	@uv publish

docs: ## Build a documentation
	@uv run sphinx-build -aq docs public

ruff: ## Run the ruff utility
	@uv run ruff check --diff
	@uv run ruff format --diff

mypy: ## Run the mypy utility
	@uv run mypy src tests

linting: ruff mypy ## Lint a whole project

test: ## Run the unit tests
	@uv run coverage run -m pytest -xv --junit-xml=pytest.xml

coverage: test ## Report a coverage statistics
	@uv run coverage report
	@uv run coverage xml -qo coverage.xml

clean: ## Delete all temporary files
	@find src tests -type f -name '*.py[cod]' -delete
	@find src tests -type d -name '__pycache__' -delete
