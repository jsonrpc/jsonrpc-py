[build-system]
requires = ["setuptools", "setuptools-scm"]
build-backend = "setuptools.build_meta"

[project]
name = "jsonrpc-py"
description = "Pure zero-dependency JSON-RPC 2.0 and OpenRPC implementation"
readme = "README.md"
classifiers = [
    "Development Status :: 5 - Production/Stable",
    "Environment :: Web Environment",
    "Framework :: AsyncIO",
    "Intended Audience :: Developers",
    "License :: OSI Approved :: Apache Software License",
    "License :: OSI Approved :: MIT License",
    "Operating System :: OS Independent",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3 :: Only",
    "Programming Language :: Python :: 3.12",
    "Programming Language :: Python :: 3.13",
    "Programming Language :: Python :: 3.14",
    "Programming Language :: Python :: Implementation :: CPython",
    "Topic :: Internet :: WWW/HTTP",
    "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
    "Topic :: Software Development :: Libraries :: Application Frameworks",
    "Topic :: Software Development :: Libraries :: Python Modules",
    "Typing :: Typed",
]
requires-python = ">=3.12"
keywords = ["jsonrpc", "openrpc", "asgi"]
dynamic = ["version"]

[[project.authors]]
name = "Andrew Malchuk"
email = "andrew.malchuk@yandex.ru"

[[project.maintainers]]
name = "JSON-RPC Development Group"
email = "dev@jsonrpc.ru"

[project.license]
text = "Apache-2.0 OR MIT"

[project.urls]
"Homepage" = "https://docs.jsonrpc.ru"
"Source Code" = "https://gitlab.com/jsonrpc/jsonrpc-py"
"Documentation" = "https://docs.jsonrpc.ru"
"Change Log" = "https://docs.jsonrpc.ru/changelog.html"

[project.optional-dependencies]
ujson = ["ujson"]

[dependency-groups]
dev = [
    "coverage ~=7.6.11",
    "furo ~=2024.8.6",
    "httpx ~=0.28.1",
    "mypy[faster-cache] ~=1.15.0",
    "pytest ~=8.3.4",
    "pytest-subtests ~=0.14.1",
    "ruff ~=0.9.6",
    "sphinx ~=8.1.3",
    "twine ~=6.1.0",
    "types-ujson",
    "ujson",
]

[tool.coverage.run]
omit = ["src/jsonrpc/typedefs.py"]
source = ["jsonrpc"]

[tool.coverage.paths]
source = ["src"]

[tool.coverage.report]
fail_under = 100.0
precision = 2
exclude_also = [
    "@overload",
    "def __repr__",
    "except ImportError",
    "if TYPE_CHECKING",
]

[tool.mypy]
show_column_numbers = true
strict = true

[[tool.mypy.overrides]]
module = "tests.*"
ignore_errors = true

[tool.ruff]
line-length = 140
src = ["src"]

[tool.ruff.lint]
extend-select = [
    "I",   # isort
    "UP",  # pyupgrade
    "S",   # flake8-bandit
    "B",   # flake8-bugbear
    "C4",  # flake8-comprehensions
    "PIE", # flake8-pie
    "RET", # flake8-return
    "SIM", # flake8-simplify
    "TID", # flake8-tidy-imports
    "TC",  # flake8-type-checking
    "RUF", # Ruff-specific rules
]
typing-modules = ["jsonrpc.typedefs"]

[tool.ruff.lint.isort]
combine-as-imports = true

[tool.ruff.lint.flake8-type-checking]
strict = true

# This is a fix for an issue in setuptools. See: https://github.com/pypa/setuptools/issues/4759
# This should be removed when the issue is resolved.
[tool.setuptools]
license-files = []

[tool.setuptools_scm]
version_scheme = "no-guess-dev"
local_scheme = "dirty-tag"
