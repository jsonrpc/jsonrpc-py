.. title:: OpenRPC Specification
.. meta::
  :description: Service description format for JSON-RPC protocol
  :keywords: python, asgi, jsonrpc, json, rpc, openrpc, schema

`OpenRPC <https://spec.open-rpc.org>`_ Specification
====================================================

Service description format for JSON-RPC protocol.

.. module:: jsonrpc.openrpc

OpenRPC Object
--------------

.. autoclass:: OpenRPC
  :members:

Info Object
-----------

.. autoclass:: Info
  :members:

Contact Object
^^^^^^^^^^^^^^

.. autoclass:: Contact
  :members:

License Object
^^^^^^^^^^^^^^

.. autoclass:: License
  :members:

Server Object
-------------

.. autoclass:: Server
  :members:

Server Variable Object
^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: ServerVariable
  :members:

Method Object
-------------

.. autoclass:: Method
  :members:

Content Descriptor Object
^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: ContentDescriptor
  :members:

Param Structure Object
^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: ParamStructure
  :members:

Example Pairing Object
^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: ExamplePairing
  :members:

Example Object
^^^^^^^^^^^^^^

.. autoclass:: Example
  :members:

Link Object
^^^^^^^^^^^

.. autoclass:: Link
  :members:

Error Object
^^^^^^^^^^^^

.. autoclass:: Error
  :members:

Components Object
-----------------

.. autoclass:: Components
  :members:

Tag Object
----------

.. autoclass:: Tag
  :members:

External Documentation Object
-----------------------------

.. autoclass:: ExternalDocumentation
  :members:

Undefined Type Object
---------------------

.. autoclass:: UndefinedType
  :members:
