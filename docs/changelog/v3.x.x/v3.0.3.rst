v3.0.3
======

Released on Apr 23, 2023 (`diff`_).

* Updated dependencies for development and testing.
* Updated documentation.

.. _`diff`: https://gitlab.com/jsonrpc/jsonrpc-py/-/compare/v3.0.2...v3.0.3
