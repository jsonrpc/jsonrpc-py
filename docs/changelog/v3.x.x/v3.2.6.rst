v3.2.6
======

Released on Jan 15, 2024 (`diff`_).

* Improved the objects hash calculation.

.. _`diff`: https://gitlab.com/jsonrpc/jsonrpc-py/-/compare/v3.2.5...v3.2.6
