v3.0.6
======

Released on May 2, 2023 (`diff`_).

* Coroutines in the requests are now :py:func:`asyncio.shield` from being cancelled.
* A sequence of coroutines in the batch requests are now scheduled
  into :py:class:`asyncio.TaskGroup` instead of :py:func:`asyncio.gather`.

.. _`diff`: https://gitlab.com/jsonrpc/jsonrpc-py/-/compare/v3.0.5...v3.0.6
