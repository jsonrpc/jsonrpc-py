v1.0.0
======

Released on Feb 8, 2022 (`diff`_).

* Initial public release.

.. _`diff`: https://gitlab.com/jsonrpc/jsonrpc-py/-/compare/0f09df6f...v1.0.0
