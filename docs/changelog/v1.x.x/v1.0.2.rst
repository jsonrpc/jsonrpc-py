v1.0.2
======

Released on Feb 19, 2022 (`diff`_).

* Added the ``Content-MD5`` header field calculation in the responses.
* The successful responses in the :attr:`jsonrpc.BatchResponse.json` attribute are now at the top of the list.
* Some documentation improvements.

.. _`diff`: https://gitlab.com/jsonrpc/jsonrpc-py/-/compare/v1.0.1...v1.0.2
