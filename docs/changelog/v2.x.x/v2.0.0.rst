v2.0.0
======

Released on May 8, 2022 (`diff`_).

* Removed previously deprecated code.
* Improved concurrency of invoking a non-blocking requests:
  notifications are now scheduled in a separate threads instead of invoking by the main thread.

.. _`diff`: https://gitlab.com/jsonrpc/jsonrpc-py/-/compare/v1.1.0...v2.0.0
