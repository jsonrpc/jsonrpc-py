v2.0.2
======

Released on May 21, 2022 (`diff`_).

* The ``jsonrpc.Dispatcher`` and ``jsonrpc.WSGIHandler`` mapping objects are now store
  items in the order the keys were last added.
* Added benchmarks for the future use to compare performance of the various
  frameworks (`Flask`_ is only now).

.. _`diff`: https://gitlab.com/jsonrpc/jsonrpc-py/-/compare/v2.0.1...v2.0.2
.. _`Flask`: https://pypi.org/project/flask
