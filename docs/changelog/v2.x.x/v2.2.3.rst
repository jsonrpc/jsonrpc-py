v2.2.3
======

Released on Aug 24, 2022 (`diff`_).

* Added parameters to the constructors of the ``jsonrpc.Dispatcher`` and ``jsonrpc.WSGIHandler``
  classes to be equal to :py:class:`dict`, :py:class:`collections.OrderedDict` etc.
* Some documentation improvements.
* Updated dependencies for development and testing.

.. _`diff`: https://gitlab.com/jsonrpc/jsonrpc-py/-/compare/v2.2.2...v2.2.3
