v2.2.2
======

Released on Jul 18, 2022 (`diff`_).

* Added new serializer ``jsonrpc.PickleSerializer``.

.. _`diff`: https://gitlab.com/jsonrpc/jsonrpc-py/-/compare/v2.2.1...v2.2.2
