v2.4.7
======

Released on Dec 10, 2022 (`diff`_).

* The :class:`jsonrpc.JSONSerializer` is now producing unicode payload instead of ``latin-1``.
* HTTP headers are now sorted before sending them to the server.

.. _`diff`: https://gitlab.com/jsonrpc/jsonrpc-py/-/compare/v2.4.6...v2.4.7
