.. title:: JSON-RPC Python
.. meta::
  :description: JSON-RPC Python framework documentation
  :keywords: python, asgi, jsonrpc, json, rpc

JSON-RPC Python
===============

.. image:: https://img.shields.io/gitlab/pipeline-status/jsonrpc/jsonrpc-py?branch=main&logo=gitlab&style=flat-square
  :target: https://gitlab.com/jsonrpc/jsonrpc-py
  :alt: pipeline status

.. image:: https://img.shields.io/gitlab/pipeline-coverage/jsonrpc/jsonrpc-py?branch=main&logo=gitlab&style=flat-square
  :target: https://gitlab.com/jsonrpc/jsonrpc-py
  :alt: coverage report

.. image:: https://img.shields.io/pypi/v/jsonrpc-py?color=steelblue&logo=python&logoColor=steelblue&style=flat-square
  :target: https://pypi.org/project/jsonrpc-py
  :alt: latest release

.. image:: https://img.shields.io/pypi/dm/jsonrpc-py?color=steelblue&logo=python&logoColor=steelblue&style=flat-square
  :target: https://pypi.org/project/jsonrpc-py
  :alt: pypi downloads

**JSON-RPC Python** is a lightweight ASGI micro framework for building web applications.

Features
--------

#. Pure zero-dependency JSON-RPC 2.0 and OpenRPC implementation.
#. Simple and user-friendly API interface.
#. 100% type annotated codebase.
#. 100% test coverage, so it's production-ready.

Requirements
------------

Python 3.12 and above.

Installation
------------

The recommended way to get ``jsonrpc-py`` is to install the latest stable release via `pip <https://pip.pypa.io>`_:

.. code-block:: console

  $ pip install jsonrpc-py

User's Guide
------------

.. toctree::
  :maxdepth: 2

  quickstart
  reference
  openrpc
  deployment
  changelog

.. toctree::
  :hidden:
  :caption: Quick Links

  PyPI Releases <https://pypi.org/project/jsonrpc-py>
  Source Code <https://gitlab.com/jsonrpc/jsonrpc-py>
