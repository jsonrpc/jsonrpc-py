.. title:: Change Log
.. meta::
  :description: Change Log for JSON-RPC Python framework
  :keywords: python, asgi, jsonrpc, json, rpc, changelog, changes, release, notes

Change Log
==========

This project follows the `semantic versioning`_ and `pre-release versioning`_ schemes
recommended by the Python Packaging Authority.

.. toctree::
  :maxdepth: 1

  changelog/v5.x.x/index
  changelog/v4.x.x/index
  changelog/v3.x.x/index
  changelog/v2.x.x/index
  changelog/v1.x.x/index

.. _`semantic versioning`: https://packaging.python.org/en/latest/discussions/versioning/#semantic-versioning-vs-calendar-versioning
.. _`pre-release versioning`: https://packaging.python.org/en/latest/discussions/versioning/
